from math import ceil

no_valid_input = 'No valid input received. Please try again.'


def select_payment_model():
    print('Please select your payment model.')
    print('Enter (1) if you want to pay by hour. Enter (2) if you want to pay for distance.')
    value = input('Enter (1) or (2): ')
    if value == '1':
        print('You selected pay by hour.')
        payment_model_one()
    elif value == '2':
        print('You selected pay for distance.')
        payment_model_two()
    else:
        print(no_valid_input)
        select_payment_model()


def payment_model_one():
    print('The cost per minute is 50 cent.')
    print('How long do you want to rent the scooter?')
    str_duration = input('Please enter duration in minutes, for example "20" (without double quotes): ')
    try:
        int_duration = int(str_duration)
        if int_duration > 0:
            if int_duration <= 300:
                print('You entered ' + str_duration + ' minutes.')
                price = 0.5 * int_duration
                print('Your payment total is ' + str(price) + ' euro.')
            else:
                print('The maximum duration is 300 minutes.')
                payment_model_one()
        else:
            print(no_valid_input)
            payment_model_one()
    except ValueError:
        print(no_valid_input)
        payment_model_one()


def payment_model_two():
    print('The cost per km is 1,50 euro.')
    print('How far do you want to ride the scooter? The distance is always rounded up, for example "1.1" --> "2".')
    str_distance = input('Please enter distance in km, for example "1" or "2.5" (without double quotes): ')
    try:
        float_distance = float(str_distance)
        if float_distance > 0:
            if float_distance <= 100:
                print('You entered ' + str_distance + ' kilometers.')
                int_distance = ceil(float_distance)
                price = int_distance * 1.50
                print('Your payment total is ' + str(price) + ' euro.')
            else:
                print('The maximum distance is 100 kilometers.')
                payment_model_two()
        else:
            print(no_valid_input)
            payment_model_two()
    except ValueError:
        print(no_valid_input)
        payment_model_two()


print('Welcome to ScooTec GmbH! Your leading mobility company in Hamburg.')
name = input('Please enter your name: ')
print('Hello ' + name + '!')
select_payment_model()
